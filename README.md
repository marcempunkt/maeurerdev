<img src="src/assets/images/logo.png" align="right" height="100" />

### My portfolio website 
> written entirely in rust with the yew framework & compiled to WASM.  
> Licensed under `MIT`

<a href="https://maeurer.dev">https://maeurer.dev</a>  

<a href="https://maeurer.dev">
	<img src="readme/screenshot.png" href="maeurer.dev"/>
</a>
