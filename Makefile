init:
	@rustup target add wasm32-unknown-unknown
start:
	@trunk serve
build:
	@trunk build --release
docker_start:
	@docker run --name marc_maeurerdev --network host --rm -d marc/maeurerdev:latest
docker_build:
	@docker build -t marc/maeurerdev:latest .
