use yew::prelude::*;
use crate::AppContext;
use crate::components::svg::emojis::Robot;
use crate::ui::Title;
use crate::components::{
    ProjectLangunion,
    ProjectSprachschule,
};

#[function_component(Projects)]
pub fn projects() -> Html {
    let app_context: AppContext = use_context::<AppContext>().expect("No AppContext found!");

    fn handle_title(app_context: &AppContext) -> &'static str {
	match app_context.language.current.as_str() {
	    "de" => "Projekte",
	    "jp" => "制作",
	    "kr" => "제작물",
	    "eng" | _ => "Projects",
	}
    }

    html!{
	<>
	    <Title id="projects">
                { handle_title(&app_context) }{ " " }<Robot />
            </Title>

            <ProjectLangunion />
            <ProjectSprachschule />

            // TODO Practical introduction to text analysis with R for beginners
            // TODO Coding Blog
            // TODO Youtube
	</>
    }
}
