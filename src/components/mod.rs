#![cfg_attr(debug_assertions, allow(unused_imports))]

pub mod nav;
pub mod landing;
pub mod aboutme;
pub mod skills;
pub mod socials;
pub mod projects;
pub mod project_langunion;
pub mod project_sprachschule;
pub mod contact;
pub mod footer;
pub mod svg;

pub use nav::*;
pub use landing::*;
pub use aboutme::*;
pub use skills::*;
pub use socials::*;
pub use projects::*;
pub use project_langunion::*;
pub use project_sprachschule::*;
pub use contact::*;
pub use footer::*;
pub use svg::*;
