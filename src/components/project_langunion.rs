use yew::{function_component, use_context, html, Html};
use crate::ui::{ProjectCard, ProjectCardColor};
use crate::AppContext;

#[function_component(ProjectLangunion)]
pub fn project_langunion() -> Html {
    let app_context: AppContext = use_context::<AppContext>().expect("No AppContext found!");

    fn handle_langunion_subtitle(app_context: &AppContext) -> &'static str {
	match app_context.language.current.as_str() {
	    "de" => "Real-time Voice & Video Chat App",
	    "jp" => "音声通話とビデオ通話できるリアルタイムチャットアプリ",
	    "kr" => "영상 통화와 음성 통화를 할 수 있는 실시간 웹 커뮤니케이션 앱",
	    "eng" | _ => "Real-time voice & video chat app",
	}
    }

    fn handle_langunion_description(app_context: &AppContext) -> &'static str {
	match app_context.language.current.as_str() {
	    "de" => "Kostenlose und Privatssphäre wahrende Messaging App mit unbegrenzte Voice und Video Call Funktionalität mit der Möglichkeit alle Deine Momente in Deiner Timeline mit Deinen Freunden und Kollegen zu teilen.",
	    "jp" => "プライバシー重視のメッセンジャーで無料に音声通話やビデオ通話したりタイムラインで写真を投稿したりできます。",
	    "kr" => "프라이버시를 중시하는 메신저로, 무료로 영상통화와 음성통화가 가능하며 타임라인에 사진을 올릴 수도 있습니다.",
	    "eng" | _ => "Free & privacy oriented messenger with unlimited voice and video calls. Share all your moments with your friends and colleagues on Your Timeline.",
	}
    }

    html! {
        <ProjectCard
            img="./assets/images/babbeln.png"
            title="babbeln"
            subtitle={handle_langunion_subtitle(&app_context)}
            description={handle_langunion_description(&app_context)}
            page_link="https://langunion.com"
            source_link="https://langunion.com"
            color={ProjectCardColor::Black}
        />
    }
}
