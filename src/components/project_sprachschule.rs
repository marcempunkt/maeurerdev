use yew::{function_component, use_context, html, Html};
use crate::ui::ProjectCard;
use crate::AppContext;

#[function_component(ProjectSprachschule)]
pub fn project_sprachschule() -> Html {
    let app_context: AppContext = use_context::<AppContext>().expect("No AppContext found!");

    fn handle_sprachschule_subtitle(app_context: &AppContext) -> &'static str {
	match app_context.language.current.as_str() {
	    "de" => "Website für mein Japanisch & Koreanisch Unterricht",
	    "jp" => "日本語と韓国語の家庭教師をしている自分のウェブサイト",
	    "kr" => "저의 일본어 튜터링 사이트",
	    "eng" | _ => "Website for myself as a Japanese & Korean tutor",
	}
    }

    fn handle_sprachschule_description(app_context: &AppContext) -> &'static str {
	match app_context.language.current.as_str() {
	    "de" => "Ich unterrichte Japanisch schon seit 4 Jahren und viele meiner Schüler können schon fließend auf Japanisch Gespräche führen. In naher Zukunft möchte ich auch noch Koreanisch unterrichten.",
	    "jp" => "もう４年間くらい日本語の家庭教師をしています。生徒は10代から20代までで、殆どの生徒はもう流暢に日本人と会話ができております。",
	    "kr" => "저는 일본어 과외선생님으로 약 4년 째 일하고 있습니다. 학생들은 10-20대이고 제 대부분의 학생들은 일본사람들과 유창하게 말할 수 있습니다. 가까운 장래는 한국어도 가르치고 싶습니다.",
	    "eng" | _ => "I'm freelancing as a Japansese tutor for almost 4 years now. I mostly teach pupils ranging from teens up to 30 years old. Most of my students already are able to have conversations with natives without a problem. In the near future I'm planning to also teach Korean.",
	}
    }

    html! {
        <ProjectCard
            img="./assets/images/sprachschule_maeurer.png"
            title={html! { <>{"Sprach"} <wbr /> {"schule Mäurer"}</> }}
            subtitle={handle_sprachschule_subtitle(&app_context)}
            description={handle_sprachschule_description(&app_context)}
            page_link="https://sprachschule-mäurer.de"
            source_link="https://sprachschule-mäurer.de"
        />
    }
}
